//
//  DateToLabelConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

/// This class converts a label to be displayed in the UI
class DateToLabelConverter {
    
    private static let defaultUnavailableDate = DateConverter.ConvertToDate(Constants.unavailableDate)
    
    /** Converts label from a date
     
        @param date the date to be converted
     
        @returns a label to be presented on the UI
     */
    static func ConvertToLabel(date: NSDate?) -> String{
        
        if let date = date {
        
            if date == defaultUnavailableDate {
                return  Constants.unavailableDateLabel
            }
        }
        
        return DateConverter.ConvertToString(date)
    }
}