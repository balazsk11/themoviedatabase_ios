//
//  MovieResponseConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 19/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class converts json response to MediaResponse to be processed or presented by a ViewController class
class MovieResponseConverter {
    
    /** Converts json response to MediaResponse Obejct
     
        @param jsonResponse The response to be converted
     
        @returns MediaResponse that holds movie information
     */
    static func convertJsonToMediaResponse(jsonResponse:AnyObject!) -> MediaResponse {
        
        var serviceResponse: MediaResponse!
        
            do {
                if let  dictionary = jsonResponse as? Dictionary<String, AnyObject> {
                    if  let page = dictionary[Constants.pageTag] as? Int,
                        let totalPages = dictionary[Constants.totalPagesTag] as? Int,
                        let totalResults = dictionary[Constants.totalResultsTag] as? Int {
                        
                        var mediaResults : [MediaDataTransferObject] = []
                        
                        if let results = jsonResponse[Constants.resultsTag] as? [[String: AnyObject]]{
                            
                            for movie in results {
                                if  let overview = movie[Constants.overviewTag] as? String,
                                    let rating = movie[Constants.voteAverageTag] as? Double,
                                    let heading = movie[Constants.originalTitleTag] as? String,
                                    let pictureUri = movie[Constants.posterPathTag] as? String,
                                    let dateString = movie[Constants.releaseDateTag] as? String
                                {
                                    let releaseDate = DateConverter.ConvertToDate(dateString)
                                    
                                    let newMovie = MediaDataTransferObject(aHeading: heading, aPictureUri: pictureUri, anOverview: overview, aReleaseDate: releaseDate, aRating: rating)
                                    
                                    mediaResults.append(newMovie)
                                }
                            }
                        }
                        else {
                            throw ConverterError.ContentFailure
                        }
                        
                        serviceResponse = MediaResponse(page: page, totalResults: totalResults, totalPages: totalPages, results: mediaResults)
                    }
                }
                else {
                    throw ConverterError.ServerResponseFailure
                }
                
            } catch {
                serviceResponse = MediaResponse(page: 0, totalResults: 0, totalPages: 0, results: [])
            }
        return serviceResponse
    }
}