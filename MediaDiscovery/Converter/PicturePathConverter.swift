//
//  PicturePathConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class converts strings to full path that can be dowladed from the internet
class PicturePathConverter {
    
    private static let baseUrl = ServiceLocations.picturePathbaseUrl
    
    /** Creates full path for a relative pictrue path
     
        @param pictureUrl The id of a picture
     
        @return returns picture full path in string format
     */
    static func getPictureFullPath(pictureUrl: String!)  -> String{
        
        let fullpath: String! = baseUrl + pictureUrl
        
        return fullpath!
    }
    
    /** Creates full path for a relative pictrue path
     
     @param relativeUrl The id of a picture
     
     @return returns picture full path in NSURL format
     */
    static func getUrlForImage(relativeUrl:String!)->NSURL{
        let imgageFullPath: String! = getPictureFullPath(relativeUrl)
        return NSURL(string: imgageFullPath)!
    }

}