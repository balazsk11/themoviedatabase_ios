//
//  JsonToDateConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class converts string to dates and back
class DateConverter {
    
    private static let dateFormater = NSDateFormatter() 
    
    private static func initFormatter() {
        
        dateFormater.dateFormat = Constants.dateFormat
    }
    
    /** Creates a date from a string
     
     @param dateString The date to be converted
     
     @return date parsed from the string
     */
    static func ConvertToDate(dateString: String?) -> NSDate {
    
        initFormatter()
        
        if let dateString = dateString {
        
            if dateString.isEmpty {
                return dateFormater.dateFromString(Constants.unavailableDate)!
            }
        }
        
        return dateFormater.dateFromString(dateString!)!
    }
    
    /** Creates a string from a date
     
     @param date The date to be converted
     
     @return date in String format parsed from NSDate
     */
    static func ConvertToString(date: NSDate!) -> String! {
        
        initFormatter()
        
        return dateFormater.stringFromDate(date)
    }

}