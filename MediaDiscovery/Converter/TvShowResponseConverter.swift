//
//  TvShowResponseConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class converts json response to MediaResponse to be processed or presented by ViewController a class
class TvShowResponseConverter {
    
    /** Converts json response to MediaResponse Obejct
     
     @param jsonResponse The response to be converted
     
     @returns MediaResponse that holds TvShow information
     */
    static func convertJsonToMediaResponse(jsonResponse:AnyObject!) -> MediaResponse {
        
        var serviceResponse: MediaResponse!
        
        do {
            if let  dictionary = jsonResponse as? Dictionary<String, AnyObject> {
                if  let page = dictionary[Constants.pageTag] as? Int,
                    let totalPages = dictionary[Constants.totalPagesTag] as? Int,
                    let totalResults = dictionary[Constants.totalResultsTag] as? Int {

                    var mediaResults : [MediaDataTransferObject] = []
                    
                    if let results = jsonResponse[Constants.resultsTag] as? [[String: AnyObject]]{

                        for tvShow in results {
                            if  let overview = tvShow[Constants.overviewTag] as? String,
                                let rating = tvShow[Constants.voteAverageTag] as? Double,
                                let heading = tvShow[Constants.originalNameTag] as? String,
                                let pictureUri = tvShow[Constants.posterPathTag] as? String,
                                let dateString = tvShow[Constants.firstAirDateTag] as? String
                            {

                                let releaseDate = DateConverter.ConvertToDate(dateString)
                                
                                let newMovie = MediaDataTransferObject(aHeading: heading, aPictureUri: pictureUri, anOverview: overview, aReleaseDate: releaseDate, aRating: rating)

                                mediaResults.append(newMovie)
                            }
                        }
                    }
                    else {
                        throw ConverterError.ContentFailure
                    }

                    serviceResponse = MediaResponse(page: page, totalResults: totalResults, totalPages: totalPages, results: mediaResults)
                }
            }
            else {
                throw ConverterError.ServerResponseFailure
            }
            
        } catch {
            serviceResponse = MediaResponse(page: 0, totalResults: 0, totalPages: 0, results: [])
        }
        return serviceResponse
    }
}