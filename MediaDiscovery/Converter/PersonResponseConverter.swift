//
//  PeopleResponseConverter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class converts json response to PeopleResponse to be processed or presented by a ViewController class
class PersonResponseConverter {
    
    /** Converts json response to PersonResponse Obejct
     
     @param jsonResponse The response to be converted
     
     @returns PersonResponse that holds Person information
     */
    static func convertJsonToMediaResponse(jsonResponse:AnyObject!) -> PersonResponse {
        
        var serviceResponse : PersonResponse!
        
        do {
            if let dictionary = jsonResponse as? Dictionary<String, AnyObject> {
                if  let page = dictionary[Constants.pageTag] as? Int,
                    let totalPages = dictionary[Constants.totalPagesTag] as? Int,
                    let totalResults = dictionary[Constants.totalResultsTag] as? Int {
                    
                    var personResult: [PersonDataTransferObject] = []
                    
                    if let results = jsonResponse[Constants.resultsTag] as? [[String: AnyObject]] {
                        
                        for person in results {
                             if let name = person["name"] as? String,
                                let popularity = person["popularity"] as? Double,
                                let picture = person["profile_path"] as? String {
                                
                                var films : [MediaDataTransferObject] = []
                                
                                if let filmography = person["known_for"] as? [[String:AnyObject]] {
                                    
                                    for film in filmography {
                                        
                                        if  let overview = film[Constants.overviewTag] as? String,
                                            let rating = film[Constants.voteAverageTag] as? Double,
                                            let heading = film[Constants.originalTitleTag] as? String,
                                            let pictureUri = film[Constants.posterPathTag] as? String,
                                            let dateString = film[Constants.releaseDateTag] as? String {
                                            
                                            let releaseDate = DateConverter.ConvertToDate(dateString)
                                            
                                            let newMovie = MediaDataTransferObject(aHeading: heading, aPictureUri: pictureUri, anOverview: overview, aReleaseDate: releaseDate, aRating: rating)
                                            
                                            films.append(newMovie)
                                        }
                                    }
                                }
                                
                                let newPerson = PersonDataTransferObject(aHeading: name, aPictureUri: picture, aPopularity: popularity, aFilmography: films)
                                
                                personResult.append(newPerson)
                            }
                        }
                    }
                    else {
                        throw ConverterError.ContentFailure
                    }
                    
                    serviceResponse = PersonResponse(page: page, totalResults: totalResults, totalPages: totalPages, results: personResult)
                }
            }
            else {
                throw ConverterError.ServerResponseFailure
            }
            
        } catch {
            serviceResponse = PersonResponse(page: 0, totalResults: 0, totalPages: 0, results: [])
        }
        
        return serviceResponse
    }
}