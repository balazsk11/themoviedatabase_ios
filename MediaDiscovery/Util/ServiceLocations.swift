//
//  ServiceLocations.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 19/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class contains constatnt fields for making web requests
class ServiceLocations {
    
    ///request url for movie discovery
    static let discoverMovies = "https://api.themoviedb.org/3/discover/movie"
    
    ///request url for Tv show discovery
    static let discoverTvShows = "https://api.themoviedb.org/3/discover/tv"
    
    ///request url for people discovery
    static let discoverPeople = "https://api.themoviedb.org/3/person/popular"
    
    ///Base url for relative Picture IDs
    static let picturePathbaseUrl = "https://image.tmdb.org/t/p/w500"
}