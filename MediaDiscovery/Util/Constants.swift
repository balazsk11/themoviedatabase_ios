//
//  Constants.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 19/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///This class contains system constatn values
class Constants {
    
    ///The segue that navigates to show movie details
    static let showMovieSegueName = "showMovie"
    
    ///The segue that navigates to show tv show details
    static let showTvShowSegueName = "showTvShow"

    ///The segue that navigates to show person details
    static let showPersonSegueName = "showPerson"
    
    ///The segue that navigates to main from person details
    static let showMainFromPersonDetailsSegueName = "backFromPersonDetails"
    
    ///The segue that navigates to main from tv show details
    static let showMainFromTvShowDetailsSegueName = "backFromTvShowDetails"
    
    ///The segue that navigates to main from tv show details
    static let showMainFromMovieDetailsSegueName = "backToMainFromMovieDetails"
    
    //The segue that navigates to movie details from person details
    static let showMovieFromPersonSegueName = "showMovieFromPerson"
    
    ///The cell name in a list that holds movies
    static let movieCellName = "movieCell"
    
    ///The cell name in a list that holds movies
    static let tvShowCellName = "tvShowCell"
    
    ///The cell name in a list that holds movies
    static let peopleCellName = "peopleCell"
    
    //The cell name in a list that holds movies according to a person
    static let personFilmCellName = "personFilmCell"
    
    ///The api key for making web request from MOvie Database
    static let apiKey = "0a08e38b874d0aa2d426ffc04357069d"
    
    ///This is a default Date value if a date could not be red from the server
    static let unavailableDate = "3000-01-01"
    
    ///This label is displayed when a date could not be applyed
    static let unavailableDateLabel = "Unavailable"
    
    ///The default format for converting dates
    static let dateFormat = "yyyy-MM-dd"
    
    //Json serialization key from "page"
    static let pageTag = "page"
    
    //Json serialization key from "total_pages"
    static let totalPagesTag = "total_pages"
    
    //Json serialization key from "total_results"
    static let totalResultsTag = "total_results"
    
    //Json serialization key from "results"
    static let resultsTag = "results"
    
    //Json serialization key from "overview"
    static let overviewTag = "overview"
    
    //Json serialization key from "vote_average"
    static let voteAverageTag = "vote_average"
    
    //Json serialization key from "original_title"
    static let originalTitleTag = "original_title"
    
    //Json serialization key from "original_name"
    static let originalNameTag = "original_name"
    
    //Json serialization key from "poster_path"
    static let posterPathTag = "poster_path"
    
    //Json serialization key from "release_date"
    static let releaseDateTag = "release_date"
    
    //Json serialization key from "first_air_date"
    static let firstAirDateTag = "first_air_date"
    
    //Json serialization key from "api_key"
    static let apiKeyTag = "api_key"
    
    //Json serialization key from "sort_by"
    static let sortByTag = "sort_by"
    
    //Request paramter that orders the results by popularity descending
    static let popularityDescParamter = "popularity.desc"
    
    //error message to be printed whe the connection is down
    static let connectionDownMessage = "Couldn't establish connection with server"
    
    //the button label on the alert dialog
    static let alertDialogButtonLabel = "OK"
    
    //the button label on the alert dialog
    static let alertDialogTitle = "I am really sorry :("
}