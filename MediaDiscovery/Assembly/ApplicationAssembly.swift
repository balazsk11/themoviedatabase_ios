//
//  ApplicationAssembly.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation
import Typhoon

///Constains methods to inject dependencies into viewControllers
public class ApplicationAssembly: TyphoonAssembly {

    ///Holds the services to be injected
    public var coreComponents: CoreComponents!
    
    ///Injects movies service into MoviesViewController
    public dynamic func moviesViewController() -> AnyObject {
        
        return TyphoonDefinition.withClass(MoviesViewControler.self) {
            (definition) in
            
            definition.injectProperty(
                #selector(CoreComponents.moviesService),
                with: self.coreComponents.moviesService())
        }
    }
    
    ///Injects tv shows service into TvShowsViewController
    public dynamic func tvShowsViewController() -> AnyObject {
        
        return TyphoonDefinition.withClass(TvShowsViewController.self){
            (definition) in
            
            definition.injectProperty(
                #selector(CoreComponents.tvShowsService),
                with: self.coreComponents.tvShowsService())
        }
    }
    
    ///Injects people service into PeopleViewController
    public dynamic func peopleViewController() -> AnyObject{
        
        return TyphoonDefinition.withClass(PeopleViewController.self) {
            (definition) in
            
            definition.injectProperty(
                #selector(CoreComponents.peopleService),
                with: self.coreComponents.peopleService())
            
        }
    }
}