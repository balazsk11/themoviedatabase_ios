//
//  CoreComponents.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation
import Typhoon

///Contains methods to inject services
public class CoreComponents: TyphoonAssembly {

    ///Injects movies service
    public dynamic func moviesService() -> AnyObject {
        return TyphoonDefinition.withClass(MoviesServiceImplementation.self)
    }
    
    ///Injects Tv shows service
    public dynamic func tvShowsService() -> AnyObject {
        return TyphoonDefinition.withClass(TvShowsServiceImplementation.self)
    }
    
    ///Injects people service
    public dynamic func peopleService() -> AnyObject {
        return TyphoonDefinition.withClass(PeopleServiceImplementation.self)
    }
    
}