//
//  ConverterError.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 26/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import Foundation

enum ConverterError : ErrorType {
 
    case ServerResponseFailure
    
    case ContentFailure
}