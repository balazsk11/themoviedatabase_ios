//
//  MediaViewModel.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 14/09/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation
import RxSwift

class MediaViewModel {
    
    private let media : MediaDataTransferObject?
    
    private let disposeBag = DisposeBag()
    
    internal private (set) var headingText : BehaviorSubject<String>
    internal private (set) var overviewText : BehaviorSubject<String>
    internal private (set) var releaseDateText : BehaviorSubject<String>
    internal private (set) var ratingText : BehaviorSubject<String>
    
    internal var photoURL: NSURL {
        return PicturePathConverter.getUrlForImage(media?.pictureUri)
    }
    
    init(media : MediaDataTransferObject) {
        self.media = media
        
        headingText = BehaviorSubject<String>(value: media.heading)
        headingText.subscribeNext{
            (heading) in
            media.heading = heading
            }.addDisposableTo(disposeBag)
        
        overviewText = BehaviorSubject<String>(value: media.overview)
        overviewText.subscribeNext{
            (overview) in
            media.overview = overview
            }.addDisposableTo(disposeBag)
        
        ratingText = BehaviorSubject<String>(value: String(media.rating))
        ratingText.addDisposableTo(disposeBag)
        
        releaseDateText = BehaviorSubject(value: DateToLabelConverter.ConvertToLabel(media.releaseDate))
        releaseDateText.addDisposableTo(disposeBag)
    }

}