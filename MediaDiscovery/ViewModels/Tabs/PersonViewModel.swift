//
//  PersonViewModel.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 14/09/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import Foundation
import RxSwift

class PersonViewModel {
    
    private let person : PersonDataTransferObject?
    
    private let disposeBag = DisposeBag()
    
    internal private (set) var nameText : BehaviorSubject<String>
    internal private (set) var ratingText : BehaviorSubject<String>
    
    internal var photoURL: NSURL {
        return PicturePathConverter.getUrlForImage(person?.pictureUri)
    }
    
    init(person: PersonDataTransferObject){
        self.person = person
        
        nameText = BehaviorSubject<String>(value: person.heading)
        nameText.subscribeNext{
            (heading) in
            person.heading = heading
            }.addDisposableTo(disposeBag)
        
        ratingText = BehaviorSubject<String>(value: String(person.popularity))
        ratingText.addDisposableTo(disposeBag)
    }
}