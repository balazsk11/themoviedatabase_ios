//
//  PersonDataTransferObject.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///Layouts business logic objects of people
class PersonDataTransferObject : DataTransferObjectBase {
    
    internal private (set) var popularity: Double
    
    internal private (set) var filmography : [MediaDataTransferObject]
    
    /** Creates an instance of "PersonDataTransferObject"
     
     @param heading: the heading of the media
     
     @param pictureUri: relative uri for the poster picture
     
     @param filmography: 3 of the most known acts
     
     @param popularity: the users rating
     */
    init(aHeading: String, aPictureUri: String, aPopularity: Double, aFilmography:[MediaDataTransferObject]){
        popularity = aPopularity
        filmography = aFilmography
        
        super.init(aHeading: aHeading, aPictureUri: aPictureUri)
    }
}