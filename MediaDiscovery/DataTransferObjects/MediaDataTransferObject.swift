//
//  MediaDataTransferObject.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///Layouts business logic objects of movie and tsv shows
class MediaDataTransferObject : DataTransferObjectBase {
    
    internal var overview:String
    
    internal var releaseDate:NSDate
    
    internal var rating:Double
    
    /** Creates an instance of "MediaDataTransferObject"
        
        @param heading: the heading of the media
     
        @param pictureUri: relative uri for the poster picture
     
        @param overview: short description of the media
     
        @param releaseDate: the premier date fro a movie or show
     
        @param rating: the users rating
     */
    init(aHeading: String, aPictureUri: String, anOverview: String, aReleaseDate: NSDate, aRating: Double){
        
        overview = anOverview
        releaseDate = aReleaseDate
        rating = aRating
        
        super.init(aHeading: aHeading, aPictureUri: aPictureUri)
    }
}