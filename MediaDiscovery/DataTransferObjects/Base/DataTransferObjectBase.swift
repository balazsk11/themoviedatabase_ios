//
//  DataTransferObjectBase.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///Base class for Data Transfer Objects
class DataTransferObjectBase {
    
    ///The title or the name of the movie or a tv show
    internal  var heading: String
    
    ///the uri for the main picture
    internal var pictureUri: String
    
    init(aHeading: String, aPictureUri: String) {
        heading = aHeading
        pictureUri = aPictureUri
    }
}