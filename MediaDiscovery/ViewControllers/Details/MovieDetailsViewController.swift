//
//  MovieDetailsViewController.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit
import AlamofireImage
import RxSwift
import RxCocoa

///this class is a ViewController that displays details about movies
class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var movieHeading: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieReleaseDateLabel: UILabel!
    @IBOutlet weak var movieRatingLabel: UILabel!
    @IBOutlet weak var movieDescriptionView: UITextView!
    
    internal var movie : MediaDataTransferObject!
    
    private var viewModel : MediaViewModel!
    
    let disposeBag = DisposeBag()
    
    //sets up initial bindings
    private func setup(viewModel: MediaViewModel) {
    
        viewModel.headingText
            .bindTo(movieHeading.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.overviewText
            .bindTo(movieDescriptionView.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.ratingText
            .bindTo(movieRatingLabel.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.releaseDateText
            .bindTo(movieReleaseDateLabel.rx_text)
            .addDisposableTo(disposeBag)
        
        movieImageView.af_setImageWithURL(viewModel.photoURL)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = MediaViewModel(media: self.movie)
        
        setup(viewModel)
    }
}
