//
//  TvShowDetailsViewController.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit
import RxSwift
import RxCocoa

///this class is a ViewController that displays details about tc shows
class TvShowDetailsViewController: UIViewController {
    
    @IBOutlet weak var tvShowHeading: UILabel!
    @IBOutlet weak var tvShowImageView: UIImageView!
    @IBOutlet weak var tvShowReleaseDateLabel: UILabel!
    @IBOutlet weak var tvShowRatingLabel: UILabel!
    @IBOutlet weak var tvShowDescriptionView: UITextView!

    var tvShow : MediaDataTransferObject!
    
    private var viewModel : MediaViewModel!
    
    let disposeBag = DisposeBag()
    
    //sets up initial bindings
    private func setup(viewModel: MediaViewModel) {
        viewModel.headingText
            .bindTo(tvShowHeading.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.overviewText
            .bindTo(tvShowDescriptionView.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.ratingText
            .bindTo(tvShowRatingLabel.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.releaseDateText
            .bindTo(tvShowReleaseDateLabel.rx_text)
            .addDisposableTo(disposeBag)
        
        tvShowImageView.af_setImageWithURL(viewModel.photoURL)
    }
    
    override func viewDidLoad() {
        
        viewModel = MediaViewModel(media: self.tvShow)
        
        setup(viewModel)
    }
}