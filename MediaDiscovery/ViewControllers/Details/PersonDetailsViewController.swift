//
//  PersonDetailsViewController.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit
import AlamofireImage
import RxSwift
import RxCocoa

///this class is a ViewController that displays details about one and one's filmography
class PersonDetailsViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var filmsCollection: UICollectionView!
    
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var personPopularity: UILabel!
    
    var person : PersonDataTransferObject!
    
    private var viewModel : PersonViewModel!
    
    let disposeBag = DisposeBag()
    
    //sets up initial bindings
    private func setup(viewModel: PersonViewModel) {
        
        viewModel.nameText
            .bindTo(personName.rx_text)
            .addDisposableTo(disposeBag)
        
        viewModel.ratingText
            .bindTo(personPopularity.rx_text)
            .addDisposableTo(disposeBag)
        
        personImageView.af_setImageWithURL(viewModel.photoURL)
    }
    
    override func viewDidLoad() {
        
        viewModel = PersonViewModel(person: self.person)
        
        setup(viewModel)
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.person.filmography.count
    }
    
    ///populates the list with data
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = filmsCollection.dequeueReusableCellWithReuseIdentifier(Constants.personFilmCellName, forIndexPath: indexPath) as! DefaultCollectionViewCell
        
        let movie = self.person.filmography[indexPath.row]
        let pictureUrl = PicturePathConverter.getUrlForImage(movie.pictureUri)
        
        cell.itemTitle?.text = movie.heading
        cell.imageView.af_setImageWithURL(pictureUrl)
        
        return cell
    }
    
    ///Fires when an item is selected from the list
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier(Constants.showMovieFromPersonSegueName, sender: self)
    }
    
    ///sets up the person to be shown
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Constants.showMovieFromPersonSegueName {
            
            let indexPaths = filmsCollection.indexPathsForSelectedItems()!
            let indexPath = indexPaths[0] as NSIndexPath
            
            let movie = self.person.filmography[indexPath.row]
            
            let destination = segue.destinationViewController as! MovieDetailsViewController

            destination.movie = movie
        }
    }
}