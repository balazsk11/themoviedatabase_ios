//
//  DefaultCollectionViewCell.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit

///This class layouts presentation model for list cells
class DefaultCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
