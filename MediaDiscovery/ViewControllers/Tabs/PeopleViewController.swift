//
//  PeopleViewController.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit

///Handles data presentation and User actions of people collection presentation
class PeopleViewController:UIViewController,UICollectionViewDelegate, UICollectionViewDataSource  {
    
    @IBOutlet weak var peopleCollection: UICollectionView!
    
    private (set) var peopleService: PeopleService!
    
    private (set) var people:[PersonDataTransferObject]
    
    required init?(coder aDecoder: NSCoder) {
        self.people = []
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        peopleService.getPeople({
            (response) in
                let peopleResult = PersonResponseConverter.convertJsonToMediaResponse(response)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.people = peopleResult.results
                self.peopleCollection.reloadData()
            })
            
        },
        errorHandler: {
            (error) in
            
            let alertController = UIAlertController(title: Constants.alertDialogTitle, message: error, preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: Constants.alertDialogButtonLabel, style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.people.count
    }
    
    ///populates the list with data
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = peopleCollection.dequeueReusableCellWithReuseIdentifier(Constants.peopleCellName, forIndexPath: indexPath) as! DefaultCollectionViewCell
        
        let person = self.people[indexPath.row]
        let pictureUrl = PicturePathConverter.getUrlForImage(person.pictureUri)
        
        cell.itemTitle?.text = person.heading
        cell.imageView.af_setImageWithURL(pictureUrl)
        
        return cell
    }
    
    ///Fires when an item is selected from the list
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier(Constants.showPersonSegueName, sender: self)
    }
    
    ///sets up the person to be shown
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Constants.showPersonSegueName {
            
            let indexPaths = peopleCollection.indexPathsForSelectedItems()!
            let indexPath = indexPaths[0] as NSIndexPath
            
            let person = self.people[indexPath.row]
            
            let destination = segue.destinationViewController as! PersonDetailsViewController
            destination.person = person
        }
    }
}
