//
//  MoviesViewControler.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit
import AlamofireImage

///Handles data presentation and User actions of Movie collection presentation
class MoviesViewControler: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var moviesCollection: UICollectionView!
    
    private (set) var movies: [MediaDataTransferObject]
    
    private (set) var moviesService: MoviesService!
    
    required init?(coder aDecoder: NSCoder) {
        self.movies = []
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moviesService.getMovies({
            (response) in
            
            let moviesResult = MovieResponseConverter.convertJsonToMediaResponse(response)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.movies = moviesResult.results
                self.moviesCollection.reloadData()
                
            })
        },
        errorHandler: {
            (error) in
            
            let alertController = UIAlertController(title: Constants.alertDialogTitle, message: error, preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: Constants.alertDialogButtonLabel, style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        })

    }
 
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.movies.count
    }
    
    ///populates the list with data
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = moviesCollection.dequeueReusableCellWithReuseIdentifier(Constants.movieCellName, forIndexPath: indexPath) as! DefaultCollectionViewCell
        
        let movie = self.movies[indexPath.row]
        let pictureUrl = PicturePathConverter.getUrlForImage(movie.pictureUri)
        
        cell.itemTitle?.text = movie.heading
        cell.imageView.af_setImageWithURL(pictureUrl)
        
        return cell
    }
    
    ///Fires when an item is selected from the list
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier(Constants.showMovieSegueName, sender: self)
    }
    ///sets up the movie to be shown
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Constants.showMovieSegueName {
            
            let indexPaths = moviesCollection.indexPathsForSelectedItems()!
            let indexPath = indexPaths[0] as NSIndexPath
            
            let movie = self.movies[indexPath.row]
            
            let destination = segue.destinationViewController as! MovieDetailsViewController
            destination.movie = movie
        }
    }
}
