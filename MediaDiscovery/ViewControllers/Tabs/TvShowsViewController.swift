//
//  TvShowsViewController.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import UIKit
import AlamofireImage

///Handles data presentation and User actions of Tv show collection presentation
class TvShowsViewController : UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var tvShowsCollection: UICollectionView!
    
    private (set) var tvShows : [MediaDataTransferObject]
    
    private (set) var tvShowsService: TvShowsService!
    
    required init?(coder aDecoder: NSCoder) {
        self.tvShows = []
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
            
        tvShowsService.getTvShows({
            (response) in
                let tvShowsResult = TvShowResponseConverter.convertJsonToMediaResponse(response)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.tvShows = tvShowsResult.results
                self.tvShowsCollection.reloadData()
            })
        },
        errorHandler: {
            (error) in
            
            let alertController = UIAlertController(title: Constants.alertDialogTitle, message: error, preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: Constants.alertDialogButtonLabel, style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.tvShows.count
    }
    
    ///populates the list with data
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = tvShowsCollection.dequeueReusableCellWithReuseIdentifier(Constants.tvShowCellName, forIndexPath: indexPath) as! DefaultCollectionViewCell
        
        let tvShow = self.tvShows[indexPath.row]
        let pictureUrl = PicturePathConverter.getUrlForImage(tvShow.pictureUri)
        
        cell.itemTitle?.text = tvShow.heading
        cell.imageView.af_setImageWithURL(pictureUrl)
        
        return cell
    }
    
    ///Fires when an item is selected from the list
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier(Constants.showTvShowSegueName, sender: self)
    }
    
    ///sets up the Tv Show to be shown
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Constants.showTvShowSegueName {
            
            let indexPaths = tvShowsCollection.indexPathsForSelectedItems()!
            let indexPath = indexPaths[0] as NSIndexPath
            
            let tvShow = self.tvShows[indexPath.row]
            
            let destination = segue.destinationViewController as! TvShowDetailsViewController
            destination.tvShow = tvShow
        }
    }
}