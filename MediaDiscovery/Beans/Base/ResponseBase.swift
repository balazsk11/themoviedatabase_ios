//
//  ResponseBase.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///Base class for "TheMovieDB" web responses
public class ResponseBase:NSObject {
    
    internal private (set) var page: Int?
    internal private (set) var totalResults: Int?
    internal private (set) var totalPages: Int?
    
    /** Creates an instance of "ResponseBase"
     
        @param page: the number of current page
     
        @param totalResults: the number of total results
     
        @param totalPages: the total number of pages
     */
    init(page: Int, totalResults: Int, totalPages: Int){
        self.page = page
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
}