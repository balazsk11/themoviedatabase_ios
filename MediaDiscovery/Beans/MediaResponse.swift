//
//  MediaResponse.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///use this class for tv show and movie responses
public class MediaResponse : ResponseBase {

    internal private (set) var results: [MediaDataTransferObject]
    
    /** Cretes an instance of "MediaResponse"
     
     @param page: the number of current page
     
     @param totalResults: the number of total results
     
     @param totalPages: the total number of pages
     
     @results: an array of MediaDataTransferObject
     */
    init(page: Int, totalResults: Int, totalPages: Int, results: [MediaDataTransferObject]){

        self.results = results
        
        super.init(page: page, totalResults: totalResults, totalPages: totalPages)
    }
}