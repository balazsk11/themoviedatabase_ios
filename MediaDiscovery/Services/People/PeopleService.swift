//
//  PeopleService.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import Foundation

///interface for injecting People
@objc public protocol PeopleService {
    
    func getPeople(completionHandler:(AnyObject)->Void, errorHandler:(String)->Void)
}