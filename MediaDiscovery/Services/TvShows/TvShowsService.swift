//
//  TvShowsService.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///interface for injecting TvShows
@objc public protocol TvShowsService {
    
    func getTvShows(completionHandler:(AnyObject)->Void, errorHandler:(String)->Void)
}