//
//  TvShowsServiceImplementation.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 21/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation
import Alamofire

///Implementation to get TvShows
class TvShowsServiceImplementation:NSObject {
    
    /** Gets Tv shows from TheMovieDatabase server
     
        @param completionHandler: a function to be called when the request is completed
     
        @param errorHandler: a function to be called win case of error occures
    */
    func getTvShows(completionHandler:(AnyObject)->Void, errorHandler:(String)->Void){
        
        Alamofire.request(
            .GET,
            ServiceLocations.discoverTvShows,
            parameters:
            [Constants.apiKeyTag: Constants.apiKey,
                Constants.sortByTag: Constants.popularityDescParamter])
            .responseJSON {
                
                response in
                
                switch response.result {
                case .Failure:
                    errorHandler(Constants.connectionDownMessage)
                case .Success:
                    completionHandler(response.result.value!)
                    
            }
        }
    }
}