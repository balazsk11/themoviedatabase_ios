//
//  MoviesService.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 18/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun! :)

import Foundation

///interface for injecting movies
@objc public protocol MoviesService {
    
    func getMovies(completionHandler:(AnyObject)->Void, errorHandler:(String)->Void)
}