//
//  ServiceUnitTests.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 28/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import XCTest
@testable import MediaDiscovery

class ServiceUnitTests : XCTestCase {
    
    func testsMoviesService() {
        
        let asyncExpectation = expectationWithDescription("getting movies")
        var hasErrors = true
        
        
        let service = MoviesServiceImplementation()
        
        service.getMovies({
            (response) in
            
            asyncExpectation.fulfill()
            hasErrors = false
            
            }, errorHandler: {
                (error) in hasErrors = true
        })
        
        self.waitForExpectationsWithTimeout(30) { error in
            
            XCTAssertNil(error)
            XCTAssertFalse(hasErrors)
        }
    }
    
    
    func testsTvShowsService() {
        
        let asyncExpectation = expectationWithDescription("getting movies")
        var hasErrors = true
        
        
        let service = TvShowsServiceImplementation()
        
        service.getTvShows({
            (response) in
            
            asyncExpectation.fulfill()
            hasErrors = false
            
            }, errorHandler: {
                (error) in hasErrors = true
        })
        
        self.waitForExpectationsWithTimeout(30) { error in
            
            XCTAssertNil(error)
            XCTAssertFalse(hasErrors)
        }
    }
    
    func testsPeopleService() {
        
        let asyncExpectation = expectationWithDescription("getting movies")
        var hasErrors = true
        
        
        let service = PeopleServiceImplementation()
        
        service.getPeople({
            (response) in
            
            asyncExpectation.fulfill()
            hasErrors = false
            
            }, errorHandler: {
                (error) in hasErrors = true
        })
        
        self.waitForExpectationsWithTimeout(30) { error in
            
            XCTAssertNil(error)
            XCTAssertFalse(hasErrors)
        }
    }

}