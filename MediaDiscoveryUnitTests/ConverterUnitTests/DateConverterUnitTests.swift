//
//  DateConverterUnitTests.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 27/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import XCTest
@testable import MediaDiscovery

class DateConverterUnitTests : XCTestCase {
    
    func testConvertToDate() {
        let formatter = MockDateFormatter.getDateFormatter()
        let dateString = UnitTestConstants.exampleDate
        
        let resultDate = DateConverter.ConvertToDate(dateString)
        let expectedDate = formatter.dateFromString(dateString)
        
        XCTAssertNotNil(resultDate)
        XCTAssertEqual(expectedDate, resultDate)
    }
    
    func testEmptyStringToDate() {
        let formatter = MockDateFormatter.getDateFormatter()
        let dateString = ""
        
        let resultDate = DateConverter.ConvertToDate(dateString)
        let expectedDate = formatter.dateFromString(Constants.unavailableDate)
        
        XCTAssertNotNil(resultDate)
        XCTAssertEqual(expectedDate, resultDate)
    }
    
    
    func testConvertToString() {
        let datesString = UnitTestConstants.exampleDate
        let formatter = MockDateFormatter.getDateFormatter()
        let date = formatter.dateFromString(datesString)
        
        let resultString = DateConverter.ConvertToString(date)
        let expectedString = UnitTestConstants.exampleDate
        
        XCTAssertNotNil(resultString)
        XCTAssertEqual(expectedString!, resultString)
    }
}