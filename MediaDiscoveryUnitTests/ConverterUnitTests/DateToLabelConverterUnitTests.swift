//
//  DateToLabelConverterUnitTests.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 27/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun :)

import XCTest
@testable import MediaDiscovery

class DateToLabelConverterUnitTests : XCTestCase {
    
    func testDateToLabelConversion() {
        
        let date = NSDate()
        
        let result = DateToLabelConverter.ConvertToLabel(date)
        let expectedResult = DateConverter.ConvertToString(date)
        
        
        XCTAssertNotNil(date)
        
        XCTAssertEqual(expectedResult, result)
    }
    
    func testUnavailableDateToLabelConversion() {
        
        let date = DateConverter.ConvertToDate(Constants.unavailableDate)
        
        let result = DateToLabelConverter.ConvertToLabel(date)
        let expectedResult = Constants.unavailableDateLabel
        
        XCTAssertNotNil(date)
        
        XCTAssertEqual(expectedResult, result)
    }
}