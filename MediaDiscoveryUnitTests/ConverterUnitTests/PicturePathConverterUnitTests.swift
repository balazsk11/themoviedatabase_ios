//
//  PicturePathConverterUnitTests.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 27/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun :)

import XCTest
@testable import MediaDiscovery

class PicturePathConverterUnitTests : XCTestCase {
    
    func testPictureFullPath() {
    
        //Arrange
        let pictureId = UnitTestConstants.fakePictureId
        //Act
        let pictureFullPath = PicturePathConverter.getPictureFullPath(pictureId)
        let expectedPath = ServiceLocations.picturePathbaseUrl + pictureId
        
        //Assert
        XCTAssertNotNil(pictureFullPath)
        XCTAssertEqual(expectedPath, pictureFullPath)
    }
    
    func testGetUrlForImage() {
        
        let pictureId = UnitTestConstants.fakePictureId
        
        let pictureURL = PicturePathConverter.getUrlForImage(pictureId)
        let expectedPath = ServiceLocations.picturePathbaseUrl + pictureId
        let expectedURL = NSURL(string: expectedPath)
        
        XCTAssertNotNil(expectedPath)
        XCTAssertNotNil(expectedURL)
        XCTAssertEqual(expectedURL, pictureURL)
    }
}