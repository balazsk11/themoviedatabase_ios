//
//  UnitTestConstants.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 27/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//
// Coding is fun :)

import Foundation

class UnitTestConstants {
    
    static let fakePictureId : String! = "realtiveID.jpeg"
    
    static let exampleDate : String! = "2016-06-06"
}