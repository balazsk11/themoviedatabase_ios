//
//  MockDateFormatter.swift
//  MediaDiscovery
//
//  Created by Balázs Koncz on 27/08/16.
//  Copyright © 2016 koncz. All rights reserved.
//

import Foundation
@testable import MediaDiscovery

class MockDateFormatter {
    
    static func getDateFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter()
        formatter.dateFormat = Constants.dateFormat
        
        return formatter
    }
}